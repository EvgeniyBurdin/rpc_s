# rpc-server

## Уникальное имя сервера

Настроки загружаются в `settings.py` из "переменных среды".

Для этих переменных должна обеспечиваться уникальность имен. Эту уникальность предоставляет "префикс" для всех "переменных среды", а именно - уникальное имя сервера плюс два символа подчеркивания `'__'`.

Уникальное имя сервера устанавливается в начале файла `settings.py` в константу `UNIQ_SERVER_NAME` из переменной среды `UNIQ_SERVER_NAME`:

```python
from os import getenv

UNIQ_SERVER_NAME = getenv('UNIQ_SERVER_NAME')

# Префикс для всех остальных переменных среды
# (для обеспечения уникальности переменных среды в ОС)
prefix = UNIQ_SERVER_NAME + '__'
...
```

**Далее описывается один  из способов доставки переменных в среду перед запуском приложения (или тестирования).**

## Переменные среды

Переменные среды хранятся в файлах с расширением `env` в папке `env/`.

Пример файла с переменными среды:

`service.env`

```env
UNIQ_SERVER_NAME=UNIQSERVERNAME

UNIQSERVERNAME__RPC_SERVER_HOST=0.0.0.0
UNIQSERVERNAME__RPC_SERVER_PORT=5071
UNIQSERVERNAME__RPC_SERVER_ENDPOINT_URL=/

UNIQSERVERNAME__DB_POSTGRES_HOST=192.168.1.1
UNIQSERVERNAME__DB_POSTGRES_PORT=5432
UNIQSERVERNAME__DB_POSTGRES_NAME=rpcs
UNIQSERVERNAME__DB_POSTGRES_USER=postgres
UNIQSERVERNAME__DB_POSTGRES_PASSWORD=XXXX
```

## Запуск скрипта

Запуск скрипта `service.py`с предварительной загрузкой переменных среды из `service.env`:

1. Добавьте путь к запускаемому скрипту в `PYTHONPATH`
2. Активируйте виртуальную среду
3. Перейдите в папку содержащую `script.py`
4. В терминале выполните команду:

```bash
source env/service.env && export $(cut -d= -f1 env/service.env) && python service.py
```

или, для запуска тестов:

```bash
source env/tests.env && export $(cut -d= -f1 env/tests.env) &&  pytest tests
```
