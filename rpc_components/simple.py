"""
    Класс SimpleRpcComponent.
    Используется только при разработке.
"""

from dataclasses import dataclass

from data_classes.base_api import BaseApiDC
from rpc_components.parent import Component as RpcComponent
from rpc_components.parent import api_method


@dataclass
class InfoInputDC(BaseApiDC):
    """
        Док к вх параметрам

        :info: Информация о чем-то
    """
    info: str


@dataclass
class InfoOutputDC(BaseApiDC):
    """
        Док к вых параметрам
        :info: Информация о чем-то
    """
    info: str


class SimpleRpcComponent(RpcComponent):
    """
        1.1. Док строка к SimpleRpcComponent.
        1.2. Вторая строка.
    """

    name = 'simple'

    @api_method('info', input_class=InfoInputDC, output_class=InfoOutputDC)
    def method_of_any_name1(self, *args, **kwargs) -> dict:
        """
            Док к показательному методу.
        """
        result = {"info": "test_value"}

        return result


if __name__ == "__main__":

    print(SimpleRpcComponent.get_api_doc())
