from dataclasses import dataclass
from typing import Optional, Union, List, Dict

from data_classes.base_api import BaseApiDC
from data_classes.base_validating import BaseValidating
from rpc_components.parent import Component as RpcComponent
from rpc_components.parent import api_method


@dataclass
class Fields(BaseApiDC):
    """
    field могут быть списком из значений

    <ul>
    <li><code>id</code> – id шаблона</li>
    <li><code>title</code> – название шаблона</li>
    <li><code>profile_id</code> – id родительского профиля</li>
    <li><code>suite_id</code> – id родительского набора</li>
    <li><code>content</code> – всё содержимое шаблона</li>
    <li><code>request</code> – часть с вопросами</li>
    <li><code>response</code> – часть с ответами</li>
    <li><code>comment</code> – часть с комментирями</li>
    </ul>

    include – true значение включает в поиск, false - выключает.
    """
    field: str
    include: bool

@dataclass
class TemplateFields(Fields):
    """
    Поля шаблонов для поиска.
    field могут быть списком из значений

    <ul>
    <li><code>id</code> – id шаблона</li>
    <li><code>profile_id</code> – id родительского профиля</li>
    <li><code>suite_id</code> – id родительского набора</li>
    <li><code>content</code> – всё содержимое шаблона</li>
    <li><code>request</code> – часть шаблона с вопросами</li>
    <li><code>response</code> – часть шаблона с ответами</li>
    </ul>

    include – true значение включает в поиск, false - выключает.
    """


@dataclass
class TemplateModifiers(Fields):
    """
    Модификаторы шаблонов.
    <ul>
    <li><code>dict_references</code> – включать или игнорировать
    упоминания словарей в шаблонах</li>
    </ul>

    include – true значение включает в поиск, false - выключает.
    """


@dataclass
class DictionaryFields(Fields):
    """
    field могут быть списком из значений

    <ul>
    <li><code>id</code> – id шаблона</li>
    <li><code>profile_id</code> – id привязанных профилей</li>
    </ul>

    include – true значение включает в поиск, false - выключает.
    """


@dataclass
class DictionaryModifiers(Fields):
    """
    Модификаторы словарей
    """


@dataclass
class SearchTemplatesScopeItems(BaseApiDC):
    """
    fields могут быть списком из полей шаблона, по которым ведется поиск.
    При отсутствии или пустом параметре это все доступные поля шаблона.
    <br><br>
    При включении параметра include=true остальные по-умолчанию включенные становятся выключенными.
    При выключении параметра include=false остальные по-умолчанию включенные остаются включенными.
    <br><br>

    modifiers могут быть списком из модификаторов:
    """
    fields: Optional[List[TemplateFields]]
    modifiers: Optional[List[TemplateModifiers]]


@dataclass
class DictionaryFields(BaseApiDC):
    """
    fields могут быть списком из полей словаря, по которым ведется поиск.
    При отсутствии или пустом параметре это все доступные поля словаря.
    <br><br>
    При включении параметра include=true остальные по-умолчанию включенные становятся выключенными.
    При выключении параметра include=false остальные по-умолчанию включенные остаются включенными.
    <br><br>

    modifiers могут быть списком из модификаторов:
    """
    fields: Optional[List[DictionaryFields]]
    modifiers: Optional[List[DictionaryModifiers]]


@dataclass
class SearchDictinariesScopeItems(BaseApiDC):
    """ Поля словарей """
    fields: List[DictionaryFields]


@dataclass
class SearchScopes(BaseApiDC):
    """
    Пространство по которому осуществляем поиск. Для поиска по шаблонам
    значение <code>templates</code> должно быть не null. Для поиска
    по словарям значение <code>dictionaries</code> должно быть не null.
    """
    templates: Optional[SearchTemplatesScopeItems]
    dictionaries: Optional[SearchDictinariesScopeItems]


@dataclass
class SearchFilters(BaseApiDC):
    """
    Фильтр по профилям и наборам которые ищем
    """
    profiles: Optional[List[str]]
    suites: Optional[List[str]]


@dataclass
class SearchResults(BaseApiDC):
    """
    Фильтр по полям возвращаемых документов.
    Это на потом.
    """
    documents: [str]


@dataclass
class SearchOptions(BaseApiDC):
    """
    Опции запросов

    :exact_search: точный поиск подстроки включая пробелы и спецсимволы
    :sort_by: по какому полю сортировать результаты
    """
    exact_search: Optional[bool] = False
    sort_by: Optional[bool] = 'id'


@dataclass
class SearchInput(BaseApiDC):
    """
Примеры:
<pre><code>
{
    "scopes": {
        "templates": {
            "fields": [
                {"field: "id", "include": true},
                {"field: "title", "include": true},
                {"field: "content", "include": true}
            ]
            "modifiers": [
                {"dict_references": false}
            ]
        }
    },
    "filters": {
        "profiles": ["aeff0a78-d89a-4b72-9f66-d58af246f1bd"]
    },
    "results" {
       "documents": {}
    },
    "options": {
        "exact_search": true
    }
}
</code></pre>


Пример поиска по вопросам: <pre><code>
{
    "scopes": {
        "templates": {
            "fields": [
                {"field: "request", "include": true}
            ]
            "modifiers": [
                {"dict_references": false}
            ]
        }
    },
    "filters": {
        "profiles": ["aeff0a78-d89a-4b72-9f66-d58af246f1bd"]
    },
    "results" {
       "documents": {}
    },
    "options": {
        "exact_search": true
    }
}
</code></pre>

Пример со словарями: <pre><code>
{
    "scopes": {
        "dictionaries": {}
    },
    "filters": {
        "profiles": ["aeff0a78-d89a-4b72-9f66-d58af246f1bd"]
    },
    "options": {
        "whole_word": true
    }
}
</code></pre>

    """
    scopes: SearchScopes
    filters: SearchFilters
    results: SearchResults
    options: SearchOptions


@dataclass
class SearchResultDocument(BaseApiDC):
    """ """
    type: str
    context: str


@dataclass
class SearchResultTemplateDocument(SearchResultDocument):
    """ """
    profile_id: str
    suite_id: str


@dataclass
class SearchResultDictionaryDocument(SearchResultDocument):
    """ """
    pass


@dataclass
class SearchOutput(BaseApiDC):
    """ """
    documents: List[
        Union[
            SearchResultTemplateDocument, SearchResultDictionaryDocument
        ]
    ]


@dataclass
class A:
    ww: int

class FulltextSearchComponent(RpcComponent):
    """
    Поисковый компонент.
    Все обращения на поиск проходят через гейтвей Арма.<br><br>

    <a href="https://wiki.nanosemantics.ai/pages/viewpage.action?pageId=572626740" target="_blank">Черновик из пожеланий лингвистов (Сonfluence)</a>
    """

    name = 'fulltext'

    @api_method('search', input_class=SearchInput, output_class=SearchOutput)
    def search(self, *args, **kwargs) -> dict:
        """
        Общий метод поиска по шаблонам и словарям.<br>
        """
        result = {'info': 'test_value'}

        return result


if __name__ == "__main__":
    import json
    print(json.dumps(FulltextSearchComponent.get_api_doc(), indent=4,
                     default=repr, ensure_ascii=False))
