"""
    В модуле определены базовый класс для rpc-компонентов и декоратор для
    API-методов.
"""
import functools
from typing import Callable, Dict

# Константы для имен новых атрибутов у API-методов
API_NAME = '_api_RPC__name_'
API_INPUT_CLASS = '_api_RPC__input_class_'
API_OUTPUT_CLASS = '_api_RPC__output_class_'


class Component:
    """
        Базовый класс rpc-компонента
    """

    # Имя компонента, используется для формирования полного имени api-метода
    name = None

    @classmethod
    def _make_full_api_name(cls, method_name) -> str:
        """
            Возвращает полное имя api-метода
        """
        return cls.name + '.' + method_name

    def get_api_methods(self) -> Dict[str, Callable]:
        """
            Сканирует все атрибуты класса, находит среди них api-методы,
            формирует из них словарь и возвращает его.
        """

        result = {}

        for attr_name in dir(self):
            attr = getattr(self, attr_name)
            if callable(attr):
                api_name = getattr(attr, API_NAME, None)
                if api_name is not None:
                    full_api_name = self._make_full_api_name(api_name)
                    result[full_api_name] = attr

        return result

    @classmethod
    def get_api_doc(cls):
        """
            Отдает список с документацией по API методам компонента
        """
        methods = cls.get_api_methods(cls)

        api_doc = []

        for method_name, method in methods.items():

            method_info = {}

            method_info['method_name'] = method_name
            method_info['method_doc_str'] = method.__doc__.strip()

            input_class = getattr(method, API_INPUT_CLASS, None)
            output_class = getattr(method, API_OUTPUT_CLASS, None)

            method_info['input_dc'] = input_class.get_doc()
            method_info['output_dc'] = output_class.get_doc()

            api_doc.append(method_info)

        return api_doc


def api_method(name, input_class=None, output_class=None, do_check=False):
    """
        Декоратор для api-методов у класса компонента
    """
    def decorator(method):

        # Добавим к методу api-атрибуты
        method.__dict__[API_NAME] = name
        method.__dict__[API_INPUT_CLASS] = input_class
        method.__dict__[API_OUTPUT_CLASS] = output_class

        @functools.wraps(method)
        def wrapped(self, *args, **kwargs):

            # TODO: Доделать, после того как решим что проверять
            # if do_check and input_class is not None:
            #    # Проверяем входные данные
            #    items = args if args else [kwargs, ]
            #    for item in items:
            #        input_class(**item)

            result = method(self, *args, **kwargs)

            # TODO: Доделать, после того как решим что проверять
            # if do_check and output_class is not None:
            #    # Проверяем ВЫходные данные
            #    items = result if isinstance(result, list) else [result, ]
            #    for item in items:
            #        output_class(**item)

            return result

        return wrapped

    return decorator
