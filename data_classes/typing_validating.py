"""
    Dataclass с проверкой типов из typing.

    Расширяет родительский класс проверками аннотаций из константы STR_ALIASES.
"""

from dataclasses import dataclass
from typing import List, Optional, Union, Callable

from data_classes.base_validating import BaseValidating, ValidatingError

# Поддержка валидации алиасов типов из typing реализуется при помощи парсинга
# строкового представления типа поля.

# Строковые представления для всех поддерживаемых алиасов
STR_ALIASES = {
    List: str(List),
    Union: str(Union),
    Optional: str(Union),  # Представление как и у Union
}


class TypingNotSupported(ValidatingError):
    pass


@dataclass
class TypingValidating(BaseValidating):
    """
        Dataclass с проверкой типов из typing.

        Расширяет родительский класс проверками аннотаций из
        константы STR_ALIASES.
    """
    def _is_instance(self, field_value, field_type) -> bool:

        str_field_type = str(field_type)

        if self._is_typing_alias(str_field_type):
            if self._is_supported_alias(str_field_type):
                # Если field_type - поддерживаемый алиас, то получим метод
                # с реализацией его валидации
                is_instance = self._get_alias_method(str_field_type)
                return is_instance(field_value, field_type)
            else:
                raise TypingNotSupported(
                    '%s - not supported' % str_field_type
                )

        return super()._is_instance(field_value, field_type)

    def _is_typing_alias(self, str_field_type):

        str_alias = list(STR_ALIASES.values())[0]
        prefix = str_alias[:str_alias.find('.')]
        return str_field_type.startswith(prefix)

    def _is_supported_alias(self, str_field_type) -> bool:
        """
            Проверяет является ли field_type поддерживаемым алиасом
        """
        for str_alias in STR_ALIASES.values():
            if str_field_type.startswith(str_alias):
                return True

        return False

    def _get_alias_method(self, str_field_type) -> Optional[Callable]:
        """
            Возавращает метод для проверки алиаса
        """

        if str_field_type.startswith(STR_ALIASES[Union]):
            return self._is_union_instance

        elif str_field_type.startswith(STR_ALIASES[List]):
            return self._is_list_instance

    def _is_union_instance(self, field_value, field_type) -> bool:
        """
            Валидация на алиасы Optional и Union.

            Проверяет является ли field_value экземпляром одного из типов из
            кортежа field_type.__args__
        """
        # У Union допустимые типы перечислены в кортеже __args__
        for item_type in field_type.__args__:
            if self._is_instance(field_value, item_type):
                return True

        # Нет ни одного типа, подходящего для field_value
        return False

    def _is_list_instance(self, field_value, field_type) -> bool:
        """
            Валидация на алиас List.

            Проверяет является ли field_value списком экземпляров field_type.
        """
        if isinstance(field_value, list):
            # У List допустимый тип стоит первым в кортеже __args__
            # (и вообще, кортеж __args__ у него всегда длиной 1)
            field_type = field_type.__args__[0]
            for item_value in field_value:
                if not self._is_instance(item_value, field_type):
                    return False

            # Все элементы списка field_value валидные
            return True
        else:
            # field_value не является списком
            self._field_is_not_instance_log.append((field_value, field_type))
            return False


if __name__ == "__main__":

    from typing import Any

    @dataclass
    class CCC(TypingValidating):
        i: Union[List[Union[bool, List[Optional[int]]]], int, List[dict]]
        p: int
        a: Any

    c = CCC(i=[['s']], p='s', a=1)
    print(c.is_valid())
    for err in c.get_errors():
        print(err)
