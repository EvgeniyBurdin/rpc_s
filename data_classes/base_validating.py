"""
    Базовый Dataclass с проверкой типов
"""
from collections import namedtuple
from dataclasses import _MISSING_TYPE, dataclass, fields
from typing import List, Optional

from utils import doc_string_parser, type_readable_form


class ValidatingError(Exception):
    pass


FieldInfo = namedtuple('Field', ['field_name', 'field_value', 'field_type'])


@dataclass
class BaseValidating:
    """
        ДатаКласс с проверкой типов данных.

        При аннотации полей датакласса можно использовать стандартные типы
        python и классы приложения.

        После создания экземпляра запускается валидация всех полей.
        Найденные ошибки "складываются" в список словарей.

        Публичные методы:

            get_errors(self) -> Optional[list]:
                Если есть поля с ошибками валидации, то вернет список со
                словарями где будут все поля и их ошибки.
                Если все поля валидные - вернет None.
                (метод можно использовать сразу после создания экземпляра
                датакласса для определения его валидности)

            is_valid(self) -> bool:
                Запускает проверку всех полей.
                Если все поля валидные - вернет True.
                Иначе - вернет False, а информация об ошибочных полях будет
                доступна в списке словарей, который можно получить при помощи
                метода get_errors(self).
                С помощь is_valid() можно повторно валидировать поля в
                экземпляре датакласса.

            get_doc(self) -> dict:
                Метод возращает словарь с документацией класса, вида: {
                    'class_name': имя_класса,
                    'class_doc_str': строка_документации,
                    'class_fields':[
                        {
                            'field_name': имя_поля,
                            'field_type': строка с типом_поля,
                            'has_default': имеет_ли_default (True/False),
                            'field_default': значение по умолчанию (если есть),
                            'field_remark': примечание_к_полю,
                            'field_position': позиция поля
                        }
                    ]
                }
    """

    def __post_init__(self):
        """
            Запускается после создания экземпляра
        """
        self._check_fields_types()

    def is_valid(self) -> bool:

        self._check_fields_types()

        return not bool(self._fields_errors)

    @classmethod
    def get_doc(cls):

        result = {}

        result['class_name'] = cls.__name__

        doc_string = doc_string_parser(cls.__doc__)

        result['class_doc_str'] = doc_string.cleared_doc_string
        doc_string_params = doc_string.params
        doc_string_params = {
            param.name: {
                'remark': param.remark,
                'position': poposition + 1
            }
            for poposition, param in enumerate(doc_string_params)
        }

        class_fields = []
        for field in fields(cls):

            field_info = {}

            field_info['field_name'] = field.name
            field_info['field_type'] = type_readable_form(field.type)

            if isinstance(field.default, _MISSING_TYPE):
                field_info['has_default'] = False
            else:
                field_info['has_default'] = True
                field_info['field_default'] = field.default

            param = doc_string_params.get(field.name, False)

            remark = ''
            position = 1000

            if param:
                remark = param['remark']
                position = param['position']

            field_info['field_remark'] = remark
            field_info['field_position'] = position

            class_fields.append(field_info)

        class_fields.sort(key=lambda x: x['field_position'])

        result['class_fields'] = class_fields

        return result

    def get_errors(self) -> Optional[list]:

        return self._fields_errors if self._fields_errors else None

    def _is_instance(self, field_value, field_type) -> bool:

        result = isinstance(field_value, field_type)

        if not result:
            self._field_is_not_instance_log.append((field_value, field_type))

        return result

    def _get_fields(self) -> List[FieldInfo]:
        """
            Возвращает список всех полей у датакласса.
            Элемент списка - именованный кортеж (имя, значения, тип) поля.
        """
        result = []
        for field in fields(self):
            field_value = self.__getattribute__(field.name)
            result.append(FieldInfo(field.name, field_value, field.type))

        return result

    def _check_fields_types(self) -> None:

        """
            Проверяет типы данных у всех полей экземпляра.

            Если значение поля не соответствует его типу, заполняется словарь
            с информацией об ошибке, а сам словарь добавляется в список.
        """

        self._fields_errors = []

        for item in self._get_fields():

            self._field_is_not_instance_log = []
            self._field_exception = None

            try:
                is_instanse = self._is_instance(
                    item.field_value, item.field_type
                )

            except Exception as error:
                is_instanse = False
                self._field_exception = error

            if not is_instanse:
                self._fields_errors.append({
                    'field_name': item.field_name,
                    'field_value': item.field_value,
                    'field_type': item.field_type,
                    'is_not_instance_log': self._field_is_not_instance_log,
                    'exception': self._field_exception
                })


if __name__ == "__main__":
    from typing import List

    class A:
        pass

    @dataclass
    class Test(BaseValidating):
        """
           Датакласс для тестовых запусков.
           Создан только для примера.
           :i: Тестовый параметр 1.
           :a: Тестовый параметр 2.
        """
        i: List[A]
        a: str = 'def_string'

    print('\n', Test.get_doc(), '\n')

    t = Test([1])
    print(t.is_valid())
    print(t.get_errors())
