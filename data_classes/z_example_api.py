from dataclasses import asdict, dataclass
from typing import List, Union

from data_classes.base_api import BaseApiDC


@dataclass
class RequestApiDC(BaseApiDC):
    """
        Шаблон запроса
    """
    jsonrpc: str
    method: str
    params: Union[dict, list]
    id: int = 1


@dataclass
class ResultApiDC(BaseApiDC):
    """
        Шаблон для ключа "result" в ответе
    """
    status: bool
    response: Union[dict, list]


@dataclass
class ResponseApiDC(BaseApiDC):
    """
        Шаблон ответа
    """
    jsonrpc: str
    result: Union[ResultApiDC, List[ResultApiDC]]
    id: int


if __name__ == "__main__":

    request = RequestApiDC(
        jsonrpc='2.0', method='test', params=['test', 'test']
    )
    print('\n', request)
    print(asdict(request))
    print(request.as_json_string())

    request = RequestApiDC(
        jsonrpc='2.0', method='test', params={'test': 'test'}
    )
    print('\n', request)
    print(asdict(request))
    print(request.as_json_string())

    print('\n- - - - - - - ')
    result = ResultApiDC(status=True, response={'foo': 'bar'})
    response = ResponseApiDC(
        jsonrpc='2.0',
        result=[
            result,
            {
                'status': True,
                'response': {'foo': 'bar'}
            }
        ], id=1
    )
    print('\n', response)
    print(asdict(response))
    print(response.as_json_string())

    result = ResultApiDC(status=True, response=['foo', 'bar'])
    response = ResponseApiDC(jsonrpc='2.0', result=result, id=1)
    print('\n', response)
    print(asdict(response))
    print(response.as_json_string())

    # =====

    @dataclass
    class ItemRpc(BaseApiDC):
        f: int

    item_1 = ItemRpc(f=1)
    item_2 = ItemRpc(f=2)

    @dataclass
    class ListRpc(BaseApiDC):
        q: Union[int, List[ItemRpc]]

    list_ = ListRpc(q=1)

    print('\n', list_)
    print(asdict(list_))
    print(list_.as_json_string())

    list_ = ListRpc(q='l')

    print('\n', list_)
    print(asdict(list_))
    print(list_.as_json_string())
