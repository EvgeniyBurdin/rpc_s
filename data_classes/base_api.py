"""
    Базовый датакласс для данных api методов json-rpc-сервера.

    Сразу после создания экземпляра происходит валидация его полей.

    Валидируются следующие аннотации:
    1. Все стандартные типы python (int, str, list и т.п.)
    2. Некоторые алиасы типов из модуля "typing", а именно:
       List, Optional, Union.
    3. Пользовательские классы.

    Класс добавляет возможность при создании использовать словарь, аналогичный
    по структуре классу-потомку BaseApiDC.

    Пример см. в секции __main__
"""

import json
from dataclasses import asdict, dataclass

from data_classes.typing_validating import TypingValidating


@dataclass
class BaseApiDC(TypingValidating):

    def _is_instance(self, field_value, field_type) -> bool:
        """
            Если в аннотации поля используется потомок BaseApiDC, то значение
            может быть аналогичным по структуре словарем.
        """
        if type(field_type) == type and issubclass(field_type, BaseApiDC):
            if isinstance(field_value, dict):
                instance = field_type(**field_value)
                errors = instance.get_errors()
                if errors is None:
                    return True
                # else:
                # TODO: Возможно, в ветке else надо реализовать
                # добавление истории ошибок/исключений из вложенного
                # датакласса в текущий

        return super()._is_instance(field_value, field_type)

    def as_json_string(self) -> str:
        """
            Отдает json-строку с полями и их значеними
        """
        return json.dumps(asdict(self))


if __name__ == "__main__":

    from typing import List

    @dataclass
    class ItemApiDC(BaseApiDC):
        iii: int

    @dataclass
    class ListItemApiDC(BaseApiDC):
        lll: List[ItemApiDC]

    item_1 = ItemApiDC(iii=1)
    item_2 = ItemApiDC(iii=2)

    # Можно создать экземпляр ListItemApiDC так:
    list_1 = ListItemApiDC(lll=[item_1, item_2])

    # или так
    list_2 = ListItemApiDC(lll=[{"iii": 1}, item_2])

    # или так
    list_3 = ListItemApiDC(lll=[{"iii": 1}, {"iii": 2}])

    # Словари из list_1, list_2 и list_3 - получаются одинаковые
    assert asdict(list_1) == asdict(list_2)
    assert asdict(list_3) == asdict(list_2)
    # ... как и json-строки
    print(list_1.as_json_string())
    print(list_2.as_json_string())
    print(list_3.as_json_string())
