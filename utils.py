import re
from dataclasses import dataclass
from typing import Any, List


@dataclass
class DocStringParam:
    """
        Параметр из строки документации.

        :name: Имя параметра.
        :remark: Примечание к параметру.
    """
    name: str
    remark: str


@dataclass
class DocStringAndParams:
    """
        Результаты парсинга строк документации.

        :cleared_doc_string:
            Строка документации с вычищенной информацией о параметрах.
        :params:
            Список распарсенных параметров с примечаниями к ним.
    """
    cleared_doc_string: str
    params: List[DocStringParam]


def doc_string_parser(doc_string: str) -> DocStringAndParams:
    """
        Функция для парсинга параметров и примечаний к ним в строке
        документации.

        Параметры и примечания к ним должны располагаться в конце строки
        документации.

        Каждый параметр должен быть заключен в символ ":" (двоеточие).

        >>>doc_string_parser(DocStringParam.__doc__)

        DocStringAndParams(cleared_doc_string='Параметр из строки
        документации.', params=[DocStringParam(name='name', remark='Имя
        параметра.'), DocStringParam(name='remark', remark='Примечание к
        параметру.')])
    """
    # Отбросим лишние символы из начала и конца
    doc_string = doc_string.strip()
    # Шаблон параметра в строке документации ":имя_параметра:"
    param_pattern = r':\w+:'

    # 1. Разобьем строку на части по разделителю param_pattern
    parts = re.split(param_pattern, doc_string)
    # Отбросим лишние символы из начала и конца каждой части
    parts = [part.strip() for part in parts]

    # 2. Соберем все параметры в список
    params = re.findall(param_pattern, doc_string)
    # Уберем двоеточия из начала и конца
    params = [param[1:-1] for param in params]

    # 3. Сформируем итоговые данные
    cleared_doc_string = parts.pop(0)
    params_list = [
        DocStringParam(name, remark) for name, remark in zip(params, parts)
    ]

    return DocStringAndParams(cleared_doc_string, params_list)


def value_readable_form(value: Any) -> str:
    """
        Преобразует значение к читаемому виду.
    """
    result = value

    # Если значение строка, то заключим ее в кавычки
    if isinstance(value, str):
        result = '"%s"' % value

    return result


def type_readable_form(type_: type) -> str:
    """
        Преобразует тип к читаемому виду.
        А именно - удаляет из строкового представления типа ссылки на модули,
        где определены используемые в типе классы, и другой "шум".
    """
    if type(type_) == type:
        result = type_.__name__
    else:
        result = str(type_)

    pattern_for_delete = r'[<>\w]*\.'
    result = re.sub(pattern_for_delete, '', result)

    return result
