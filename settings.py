from os import getenv

UNIQ_SERVER_NAME = getenv('UNIQ_SERVER_NAME')

# Префикс для всех рстальных переменных среды
# (для обеспечения уникальности переменных среды в ОС)
prefix = UNIQ_SERVER_NAME + '__'

RPC_SERVER_HOST = getenv(prefix + 'RPC_SERVER_HOST')
RPC_SERVER_PORT = getenv(prefix + 'RPC_SERVER_PORT')
RPC_SERVER_ENDPOINT_URL = getenv(prefix + 'RPC_SERVER_ENDPOINT_URL')

DB_POSTGRES_HOST = getenv(prefix + 'DB_POSTGRES_HOST')
DB_POSTGRES_PORT = getenv(prefix + 'DB_POSTGRES_PORT')
DB_POSTGRES_NAME = getenv(prefix + 'DB_POSTGRES_NAME')
DB_POSTGRES_USER = getenv(prefix + 'DB_POSTGRES_USER')
DB_POSTGRES_PASSWORD = getenv(prefix + 'DB_POSTGRES_PASSWORD')
