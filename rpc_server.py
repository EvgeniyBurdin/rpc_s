"""
    Классы rpc-серверов
"""
from abc import ABC, abstractmethod

from flask import Flask
from flask import Response as FlaskResponse
from flask import request as flask_request
from jsonrpcserver import dispatch
from jsonrpcserver.methods import global_methods

from settings import RPC_SERVER_ENDPOINT_URL as ENDPOINT_URL
from settings import RPC_SERVER_HOST as HOST
from settings import UNIQ_SERVER_NAME as NAME
from settings import RPC_SERVER_PORT as PORT


class NoRegisteredMethodsException(Exception):
    pass


class AbstractRpcServer(ABC):

    def __init__(self, name=NAME, host=HOST, port=PORT,
                 endpoint_url=ENDPOINT_URL,
                 methods=None) -> None:
        """
            - Устанавливает параметры сервера
            - Вызывает регистрацию rpc-методов
        """
        self.name = name
        self.host = host
        self.port = port
        self.endpoint_url = endpoint_url
        self.registration_methods(methods)

    def registration_methods(self, methods):
        """
            Инициализирует свойство self.global_methods,
            и если methods не None - регистрирует методы на сервере.
        """
        self.global_methods = global_methods
        self.global_methods.items = {}

        if methods is not None:
            # Зарегистрируем методы
            self.global_methods.add(**methods)

    def run(self):
        """
            Запускает сервер
        """
        if not self.global_methods.items:
            raise NoRegisteredMethodsException(
                'The server has no registered methods!'
            )

        self._server_run()

    def _dispatch(self, request: str):
        """
            Метод должен вызываться в обработчике запроса сервера
        """
        response = dispatch(
            request=request, methods=self.global_methods,
            debug=True, basic_logging=True
        )

        return response

    @abstractmethod
    def _server_run(self):
        """
            Запускает сервер
        """
        pass

    @abstractmethod
    def get_test_client(self):
        """
            Возвращает контекстный менеджер клиента для тестирования
            текущего rpc-сервера
        """
        pass


class FlaskRpcServer(AbstractRpcServer):

    def __init__(self, *args, **kwargs):

        super().__init__(*args, **kwargs)

        self.app = Flask(self.name)
        self._init_route()

    def _init_route(self):

        app = self.app
        @app.route(self.endpoint_url, methods=["POST"])
        def index():
            """
                Обработка всех запросов к api gateway
            """
            request = flask_request.get_data().decode()

            response = self._dispatch(request=request)

            return FlaskResponse(
                str(response), response.http_status,
                mimetype="application/json"
            )

        return index

    def _server_run(self):
        """
            Запускает сервер
        """
        self.app.run(host=self.host, port=self.port)

    def get_test_client(self):
        """
            Возвращает контекстный менеджер клиента для тестирования
            текущего rpc-сервера
        """
        return self.app.test_client
