import settings


def test_env():
    """
        Тестируем наличие настроек среды
    """
    assert settings.RPC_SERVER_HOST
    assert settings.RPC_SERVER_PORT
    assert settings.RPC_SERVER_ENDPOINT_URL

    assert settings.DB_POSTGRES_HOST
    assert settings.DB_POSTGRES_PORT
    assert settings.DB_POSTGRES_NAME
    assert settings.DB_POSTGRES_USER
    assert settings.DB_POSTGRES_PASSWORD
