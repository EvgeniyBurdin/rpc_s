from dataclasses import asdict, dataclass
from typing import List

import pytest

from data_classes.base_api import BaseApiDC


@dataclass
class ItemApiDC(BaseApiDC):
    iii: int


@dataclass
class ListItemApiDC(BaseApiDC):
    lll: List[ItemApiDC]


item_1 = ItemApiDC(iii=1)
item_2 = ItemApiDC(iii=2)

# Можно создать экземпляр ListItemApiDC так:
list_1 = ListItemApiDC(lll=[item_1, item_2])
# или так
list_2 = ListItemApiDC(lll=[{"iii": 1}, item_2])
# или так
list_3 = ListItemApiDC(lll=[{"iii": 1}, {"iii": 2}])


def test_asdict():
    """
        Тест сравнения при помощи asdict()
    """
    # Словари из list_1, list_2 и list_3 - получаются одинаковые
    assert asdict(list_1) == asdict(list_2)
    assert asdict(list_3) == asdict(list_2)


def test_as_json_string():
    """
        Тест сравнения при помощи as_json_string()
    """
    json_string_1 = list_1.as_json_string()
    json_string_2 = list_2.as_json_string()
    json_string_3 = list_3.as_json_string()

    assert json_string_1 == json_string_2
    assert json_string_3 == json_string_2


def test_is_instance():
    """
        Тест метода валидации.

        Для теста метода можно взять любой экземпляр потомка BaseApiDC.
        Здесь пусть это будет уже созданный list_1.
        (это не важно, так как мы напрямую передаем значение и тип в метод)

        При валидации, любого потомка ItemApiDC, можно заменить словарем
        с аналогичными полями/ключами.
    """
    assert list_1._is_instance([item_1], List[ItemApiDC])
    assert list_1._is_instance([{'iii': 1}], List[ItemApiDC])

    # Если передать не список словарей и не список потомков BaseApiDC
    # то вернет False
    assert not list_1._is_instance(None, List[ItemApiDC])
    assert not list_1._is_instance(['1', '2'], List[ItemApiDC])
    assert not list_1._is_instance(1, List[ItemApiDC])

    # Если передать список словарей с невалидными значениями,
    # то вернет False
    assert not list_1._is_instance(
        [{'iii': 1}, {'iii': (1, 2,)}], List[ItemApiDC]
    )

    # Если передать список словарей c неверными ключами, то поднимет
    # исключение TypeError
    with pytest.raises(TypeError):
        assert not list_1._is_instance(
            [{'wrong_param_name': 1}], List[ItemApiDC]
        )
    with pytest.raises(TypeError):
        assert not list_1._is_instance(
            [{'wrong_param_type': 'string'}], List[ItemApiDC]
        )

    with pytest.raises(TypeError):
        assert not list_1._is_instance(
            [{'iii': 1}, {2: 3, 4: 5}], List[ItemApiDC]
        )


def test_create_success():
    """
        Тест удачного создания экземпляра.
        При создании экземпляра исключений не будет.
        Об успешном создании свидетельствует None результат get_errors()
    """
    instance = ListItemApiDC(lll=[{"iii": 1}])
    assert instance.get_errors() is None
    instance = ListItemApiDC(lll=[item_1])
    assert instance.get_errors() is None


def test_create_fail():
    """
        Тест НЕудачного создания экземпляра.
        При создании экземпляра исключений не будет.
        Об ошибках свидетельствует не None результат get_errors()
    """
    instance = ListItemApiDC(lll=[{"iii_": 1}])  # Неверное имя параметра
    assert instance.get_errors() is not None

    instance = ListItemApiDC(lll=[{"iii": '1'}])  # Неверный тип параметра
    assert instance.get_errors() is not None


def test_is_valid():

    # Создадим валидный экземпляр
    instance = ListItemApiDC(lll=[{"iii": 1}])
    # Убедимся в валидности экземпляра
    assert instance.get_errors() is None

    # Изменим поле на НЕвалидное значение
    instance.lll = 1
    # Убедимся в НЕвалидности экземпляра
    assert not instance.is_valid()

    # Изменим поле на Валидное значение
    instance.lll = [{"iii": 2}, item_1]
    # Убедимся в Валидности экземпляра
    assert instance.is_valid()
