from dataclasses import dataclass

from data_classes.base_validating import (BaseValidating, FieldInfo,
                                          ValidatingError)


# ---------------------------------------------------------------------------
# Исходные данные для тестов ------------------------------------------------


class UserClass:
    """
        Простой пользовательский класс
    """
    pass


@dataclass
class ValidatingDC(BaseValidating):
    """
        Датакласс с проверкой типов.
        (для теста взяты не все python-типы, но для демонстрации достаточно)
    """
    field_int: int
    field_list: list
    field_dict: dict
    field_uc: UserClass


# Набор валидных данных для датакласса
valid_data = {
    'field_int': 1,
    'field_list': [2, 3],
    'field_dict': {4: 5, '6': '7'},
    'field_uc': UserClass(),
}


# Набор НЕвалидных данных для датакласса (все поля - с ошибкой)
invalid_data = {
    'field_int': '1',
    'field_list': 2,
    'field_dict': 3,
    'field_uc': 4,
}


# ---------------------------------------------------------------------------
# Тесты вспомогательных структур -------- -----------------------------------


def test_subclass_validating_error():
    """
        Тест родителя у класса исключения
    """
    assert issubclass(ValidatingError, Exception)


def test_namedtuple_field_info():
    """
        Тест наличия полей в именованном кортеже FieldInfo
    """
    namedtuple_fields = set(FieldInfo._fields)
    required_fields = set(('field_name', 'field_value', 'field_type'))
    assert namedtuple_fields == required_fields


# ---------------------------------------------------------------------------
# Тесты методов датакласса BaseValidating -----------------------------------


def test__post_init__():
    """
        Тест метода self.__post_init__()

        А именно - запуска self._check_fields_types() в
        методе self.__post_init__()
    """
    # У любого датакласса метод self.__post_init__() запускается после
    # создания экземпляра. В датаклассе-потомке BaseValidating, этот метод
    # должен запускать метод self._check_fields_types().
    # Для тестирования этого функционала - подменим self._check_fields_types()

    @dataclass
    class ExampleValidatingDC(ValidatingDC):

        def _check_fields_types(self):
            self.executed_check_fields_types = 'ok'

    instance = ExampleValidatingDC(**valid_data)

    assert instance.executed_check_fields_types == 'ok'


def test_is_valid():
    """
        Тест публичного метода self.is_valid()
    """
    @dataclass
    class ExampleValidatingDC(ValidatingDC):

        def __post_init__(self):
            # В классе-родителе этот метод так же запускает валидацию, сразу
            # после создания экземпляра. Для чистоты теста - отменим этот
            # функционал.
            pass

    instance = ExampleValidatingDC(**valid_data)
    # Метод запускает проверку данных и возвращает True если нет ошибок...
    assert instance.is_valid()

    instance = ExampleValidatingDC(**invalid_data)
    # ...или False если они есть
    assert not instance.is_valid()


def test_get_errors():
    """
        Тест публичного метода self.get_errors()
    """

    instance = ValidatingDC(**invalid_data)
    errors = instance.get_errors()
    # Если датакласс имеет ошибки, то этот метод вернет:
    # ...список ошибок
    assert isinstance(errors, list)
    # ...длиной, равной количеству ошибочных полей
    assert len(errors) == len(invalid_data.keys())

    instance = ValidatingDC(**valid_data)
    # Если датакласс НЕ имеет ошибок, то метод вернет None.
    assert instance.get_errors() is None


def test_is_instance():
    """
        Тест метода self._is_instance(field_value, field_type)

        Метод проверяет соответствует ли переданное значение его типу.
        Если да - возращает True.
        Если нет - возращает False и добавляет кортеж (значение, тип) в список
        лога ошибок поля self._field_is_not_instance_log.
    """
    # Создадим произвольный валидный экземпляр, для доступа к методу
    instance = ValidatingDC(**valid_data)

    # Убедимся, что список ошибок у всего класса - отсутствует.
    assert instance.get_errors() is None

    # Сделаем вызов метода с валидным значением
    instance._is_instance(1, int)
    # Убедимся, что список ошибок поля - пустой.
    assert not instance._field_is_not_instance_log

    # Сделаем вызов метода с НЕвалидным значением
    instance._is_instance(1, str)
    # Убедимся, что:
    # ...список ошибок поля - имеет теперь длину 1.
    assert len(instance._field_is_not_instance_log) == 1
    # ...в списке кортеж (1, str)
    assert instance._field_is_not_instance_log[0] == (1, str)


def test_get_fields():
    """
        Тест метода self._get_fields()

        Метод должен вернуть список именованных кортежей FieldInfo.
    """
    # Создадим произвольный экземпляр, для доступа к методу
    instance = ValidatingDC(**valid_data)

    fields = instance._get_fields()

    # Убедимся, что:
    # ...длина списка равна длине списка входных параметров
    assert len(fields) == len(valid_data.keys())
    # ...что метод вернул список в котором кортежи FieldInfo
    assert isinstance(fields[0], FieldInfo)

    # Пройдем по списку и сравним значения в нем со входящими из valid_data
    for field_name, field_value, field_type in fields:
        assert field_value == valid_data[field_name]
        assert isinstance(valid_data[field_name], field_type)


def test_check_fields_types():
    """
        Тест метода self._check_fields_types()
    """
    @dataclass
    class ExampleValidatingDC(ValidatingDC):

        def __post_init__(self):
            # В классе-родителе этот метод так же запускает
            # self._check_fields_types(), сразу после создания экземпляра.
            # Для чистоты теста - отменим этот функционал.
            pass

    # Создадим валидный экземпляр, для доступа к методу
    instance = ExampleValidatingDC(**valid_data)
    instance._check_fields_types()
    # После проверки список self._fields_errors должен быть пустой
    assert len(instance._fields_errors) == 0

    # Создадим НЕвалидный экземпляр, для доступа к методу
    instance = ExampleValidatingDC(**invalid_data)
    instance._check_fields_types()
    # После проверки длина self._fields_errors должна быть равна количеству
    # ошибочных полей во входных данных
    assert len(instance._fields_errors) == len(invalid_data.keys())


# ----------------------------------------------------------------------------
# Тесты работы датакласса BaseValidating -------------------------------------


def test_create_valid():
    """
        Тест создания валидного экземпляра
    """
    instance = ValidatingDC(**valid_data)
    assert instance.get_errors() is None


def test_create_invalid():
    """
        Тест создания НЕвалидного экземпляра
    """
    instance = ValidatingDC(**invalid_data)
    errors = instance.get_errors()

    # Длина списка с ошибками должна быть равна количеству ошибочных полей
    # во входных данных
    assert len(errors) == len(invalid_data.keys())


def test_list_errors():
    """
        Тест состава списка ошибок

        Список ошибок состоит из списка словарей вида: {
            'field_name': имя_поля_с_ошибкой,
            'field_value': значение_поля,
            'field_type': тип_поля_согласно_аннотации,
            'is_not_instance_log': список_кортежей_неудачных_isinstance,
            'exception': екземпляр_исключения_при_вызове_isinstance (если было)
        }

        Поле 'is_not_instance_log' может содержать пустой список.
        Поле 'exception' может быть None.
        Остальные поля всегда содержат значения.
    """
    instance = ValidatingDC(**invalid_data)
    errors = instance.get_errors()

    for error in errors:
        value = invalid_data[error['field_name']]
        assert error['field_value'] == value
        assert not isinstance(value, error['field_type'])
        assert error['is_not_instance_log'] == [(value, error['field_type'])]
        assert error['exception'] is None


def test_list_errors_with_exception():
    """
        Тест наличия исключения в ошибках.

        Обычно, стандартная функция isinstance(field_value, field_type)
        возвращает True или False. Но иногда может и поднять исключение.
        В этом случае - поле, при проверке которого возникло исключение,
        считается ошибочным. И информация о нем и об исключении - должна
        попасть в итоговый список ошибок.
    """
    from typing import List

    # Создадим датакласс, валидация которого поднимет исключение
    @dataclass
    class ExampleValidatingDC(BaseValidating):
        field_int: List[int]

    instance = ExampleValidatingDC(field_int=1)
    # При валидации будет вызвано isinstanse(1, List[int])), и это поднимет
    # исключение
    errors = instance.get_errors()

    # Проверим наличие исключения в ошибках
    assert isinstance(errors[0]['exception'], Exception)
