from dataclasses import dataclass, fields
from typing import List, Optional, Union

import pytest

from data_classes.typing_validating import (
    STR_ALIASES, TypingValidating, TypingNotSupported
)


def test_str_aliases():
    """
        Тест строковых представлений алиасов.
        (Обратите внимание: у Union и Optional одно и тоже представление)
    """
    assert STR_ALIASES[List] == str(List)
    assert STR_ALIASES[Union] == str(Union)
    assert STR_ALIASES[Optional] == str(Union)


@dataclass
class SimpleAllDC(TypingValidating):
    """
        Простой датакласс со всеми поддерживаемыми алиасами
    """
    list_int: List[int]
    union_int_str: Union[int, str]
    optional_int: Optional[int]


params = {
    'list_int': [1],
    'union_int_str': '2',
    'optional_int': None,
}


@pytest.fixture
def simple_all():
    """
        Возвращает экземпляр датакласса с полями всех поддерживаемых алиасов
    """
    return SimpleAllDC(**params)


def test_supported_aliases_number(simple_all):
    """
        "Тест тестов"

        Тест того, что все поля в тестовом классе SimpleAllDC перечислены в
        константе STR_ALIASES.

        Если тест падает - добавьте поля с новыми алиасами в SimpleAllDC.
    """
    assert len(fields(simple_all)) == len(STR_ALIASES)


def test_is_supported_alias(simple_all):
    """
        Тест метода проверки поддержки алиаса
    """
    # Все поля у simple_all должны поддерживаться
    for field in fields(simple_all):
        assert simple_all._is_supported_alias(str(field.type))


def test_get_alias_method(simple_all):
    """
        Тест получения метода валидации для алиаса
    """
    for alias in STR_ALIASES.keys():

        if alias == List:
            method = simple_all._is_list_instance

        elif alias == Union:
            method = simple_all._is_union_instance

        elif alias == Optional:
            method = simple_all._is_union_instance   # Такой же как у Union

        assert simple_all._get_alias_method(STR_ALIASES[alias]) == method


def test_is_union_instance(simple_all):
    """
        Тест валидации алиасов Union и Optional
    """
    assert simple_all._is_union_instance(1, Union[int, str])
    assert simple_all._is_union_instance('s', Union[int, str])

    assert not simple_all._is_union_instance(1.1, Union[int, str])

    assert simple_all._is_union_instance(1, Optional[int])
    assert simple_all._is_union_instance(None, Optional[int])

    assert not simple_all._is_union_instance('s', Optional[int])


def test_is_list_instance(simple_all):
    """
        Тест валидации алиаса List
    """
    assert simple_all._is_list_instance([1], List[int])
    assert simple_all._is_list_instance([[], ], List[list])
    assert simple_all._is_list_instance(['1', '2', '3'], List[str])

    assert not simple_all._is_list_instance(['1'], List[int])
    assert not simple_all._is_list_instance([(1, 2), ], List[list])
    assert not simple_all._is_list_instance(['1', 2, '3'], List[str])


def test_is_instance_succes(simple_all):
    """
        Тест успешного завершения метода валидации.
        (неважно что вернет True или False, главное - без исключений)
    """
    assert simple_all._is_instance(
        [1, {2: 3}],
        List[Union[int, dict]]
    )
    assert not simple_all._is_instance(
        [1, '2'],
        List[Union[int, dict]]
    )
    # ---
    assert simple_all._is_instance(
        [1, None],
        List[Optional[int]]
    )
    assert not simple_all._is_instance(
        ['1', None],
        List[Optional[int]]
    )
    # ---
    assert simple_all._is_instance(
        [1, '2'],
        Union[int, List[Union[int, str]], str]
    )
    assert not simple_all._is_instance(
        [1, None],
        Union[int, List[Union[int, str]], str]
    )
    # ---
    assert simple_all._is_instance(
        1,
        Union[int, List[Union[int, str]], str]
    )
    assert not simple_all._is_instance(
        {1: 2},
        Union[int, List[Union[int, str]], str]
    )
    # ---
    assert simple_all._is_instance(
        '1',
        Union[int, List[Union[int, str]], str]
    )
    assert not simple_all._is_instance(
        (1, ),
        Union[int, List[Union[int, str]], str]
    )


def test_raise_typing_not_supported(simple_all):
    """
        Тест поднятия исключения TypingNotSupported если используется
        неподдерживаемый алиас из typing
    """
    # Некоторые неподдерживаемые классом TypingValidating алиасы
    from typing import Any, Dict, Tuple

    with pytest.raises(TypingNotSupported):
        simple_all._is_instance(1, Any)

    with pytest.raises(TypingNotSupported):
        simple_all._is_instance({1: 1}, Dict[int, int])

    with pytest.raises(TypingNotSupported):
        simple_all._is_instance((1, ), Tuple[int])
