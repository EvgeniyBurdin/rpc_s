"""
    Тестирование абстрактного rpc-сервера
"""
import pytest

import settings
from rpc_components.simple import SimpleRpcComponent
from rpc_server import AbstractRpcServer, NoRegisteredMethodsException


class _AbstractRpcServer(AbstractRpcServer):
    """
        Класс для возможности тестирования абстрактного сервера
    """

    def _server_run(self):
        """
            "Транзит" возврата от родительского абстрактного метода
        """
        return super()._server_run()

    def get_test_client(self):
        """
            "Транзит" возврата от родительского абстрактного метода
        """
        return super().get_test_client()


def test_settings():
    """
        Тест основных настроек абстрактного сервера
    """
    simple_rpc_component = SimpleRpcComponent()
    methods = simple_rpc_component.get_api_methods()

    server = _AbstractRpcServer(methods=methods)

    assert server.name == settings.UNIQ_SERVER_NAME
    assert server.host == settings.RPC_SERVER_HOST
    assert server.port == settings.RPC_SERVER_PORT
    assert server.endpoint_url == settings.RPC_SERVER_ENDPOINT_URL
    assert methods == server.global_methods.items


def test_abstract_methods():
    """
        Тест наличия абстракных методов
    """
    server = _AbstractRpcServer()

    assert server._server_run() is None
    assert server.get_test_client() is None


def test_run_without_methods_fail():
    """
        Тест неудачного запуска при отсутствии rpc-методов
    """
    server = _AbstractRpcServer()
    assert not server.global_methods.items
    with pytest.raises(NoRegisteredMethodsException):
        server.run()


def test_dispatch():
    """
        Тест self.dispatch(request) абстрактного сервера.

        Этот метод должен принять json-строку, распарсить ее params, отдать
        параметры в method, получить от него словарь с ответом, упаковатьего
        в json-строку и отдать ее.
    """
    # Для регистрации какого-нибудь метода создадим экземпляр простого
    # rpc-компонента
    simple_rpc_component = SimpleRpcComponent()
    methods = simple_rpc_component.get_api_methods()
    server = _AbstractRpcServer(methods=methods)

    # Строка запроса
    request = '''
        {
            "jsonrpc": "2.0",
            "method": "simple.info",
            "params": {
                "info": "test_value"
            },
            "id": 1
        }
    '''
    # Ожидаемый ответ
    result = '{"jsonrpc": "2.0", "result": {"info": "test_value"}, "id": 1}'

    # Сделаем запрос
    response = server._dispatch(request)

    assert result == str(response)
