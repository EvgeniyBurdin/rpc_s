"""
    Модуль тестов для утилит
"""
from typing import List, Optional, Union

from data_classes.base_api import BaseApiDC
from utils import type_readable_form, value_readable_form, doc_string_parser


def test_doc_string_parser():
    """
        Тест парсинга параметров из строки документации
    """
    input_doc_string = '''
        Строка документации.

        :param_1: Примечание к параметру 1.
        :param_2: Примечание к параметру 2.
    '''
    doc = doc_string_parser(input_doc_string)
    assert doc.cleared_doc_string == 'Строка документации.'
    assert doc.params[0].name == 'param_1'
    assert doc.params[0].remark == 'Примечание к параметру 1.'
    assert doc.params[1].name == 'param_2'
    assert doc.params[1].remark == 'Примечание к параметру 2.'


def test_value_readable_form_for_string():
    """
        Тест заворачивание строки в кавычки для красивого представления
    """
    s = '123'
    assert value_readable_form(s) == '"123"'


def test_type_readable_form():
    """
        Тест создания читаемого представления типа
    """
    types = (
        (int, 'int'),
        (str, 'str'),
        (list, 'list'),
        (List[str], 'List[str]'),
        (Optional[BaseApiDC], 'Union[BaseApiDC, NoneType]'),
        (
            Union[int, BaseApiDC, List[BaseApiDC]],
            'Union[int, BaseApiDC, List[BaseApiDC]]'
        )
    )

    for t, result in types:
        assert type_readable_form(t) == result
