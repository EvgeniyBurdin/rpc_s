# -*- coding: utf-8 -*-
"""
    Модуль для генерации документации по методам API у rpc-сервера, и по
    датаклассам.
    (черновой, но уже рабочий вариант)

    Для корректной сборки документации необходимо:
        1. Перечислить в константе RPC_COMPONENTS все классы rpc-компонентов,
           для методов которых надо собрать документацию
        2. Перечислить в константе DC_EXTRA все датаклассы, для которых надо
           собрать документацию, но которые отсутствуют в декораторах к
           api-методам у уже перечисленных rpc-компонент.
"""

import os

from jinja2 import Environment, FileSystemLoader

from rpc_components.fulltext import (
    DictionaryFields, DictionaryModifiers, FulltextSearchComponent,
    SearchDictinariesScopeItems, SearchFilters, SearchOptions,
    SearchResultDictionaryDocument, SearchResultDocument, SearchResults,
    SearchResultTemplateDocument, SearchScopes, SearchTemplatesScopeItems,
    TemplateFields, TemplateModifiers
)
from rpc_components.simple import SimpleRpcComponent
from utils import type_readable_form, value_readable_form

# В этот список надо добавить классы датаклассов, которые не используются в
# объявлениях методов, но должны быть в документации
DC_EXTRA = (
    SearchOptions, SearchResults, SearchFilters,
    SearchScopes, SearchDictinariesScopeItems, TemplateFields,
    DictionaryFields, SearchResultTemplateDocument,
    SearchResultDictionaryDocument, SearchResultDocument,
    SearchTemplatesScopeItems, TemplateModifiers, DictionaryModifiers
)


RPC_COMPONENTS = (
    FulltextSearchComponent, SimpleRpcComponent
)

DOCS = 'docs/'
DOCS_RPC_COMPONENTS = 'rpc_components/'
DOCS_DATA_CLASSES = 'data_classes/'


def make_rpc_component_url(component_name: str) -> str:
    return DOCS_RPC_COMPONENTS + component_name + '/index.html'


def make_method_url(method_name: str) -> str:
    name = method_name.split('.')
    return name[1] + '.html'


def make_data_class_url(data_class_name: str) -> str:
    return '../../' + DOCS_DATA_CLASSES + data_class_name + '.html'


def save_doc(data: dict, template_path: str, path: str) -> None:
    env = Environment(
        loader=FileSystemLoader(
            [
                'documentation_creation/templates',
                'documentation_creation/method_examples'
            ],
        )
    )

    template = env.get_template(template_path)

    try:

        render = template.render(**data)

        if not os.path.exists(os.path.dirname(path)):
            dir_name = os.path.dirname(path)
            os.makedirs(dir_name)

        with open(path, 'w') as f:
            f.write(render)

    except Exception as error:
        print('-' * 30)
        print('Ошибка:', error)
        print('Не создана страница:', path)


def make_str_p_html(doc_str: str) -> str:
    """
        Из обычной строки формирует строку html в которой параграфы заключены
        в тэг <p>...</p>, и возвращает ее.
    """
    raw_str = doc_str.strip().split('\n')
    strings = [s.strip() for s in raw_str]

    result = '<p>'
    enter = 0
    for string in strings:
        if string:
            result += string + ' '
            enter = 0
        else:
            enter += 1

        if enter == 1:
            result += '</p>\n<p>'

    result += '</p>'

    return result


def create_doc(rpc_components=RPC_COMPONENTS):

    # Удалим всё из папки DOCS
    os.system('rm -rf %s/*' % DOCS)

    # Словарь для сбора всех датаклассов
    # Имеет вид {имя_dc: словарь_с_документацией_dc}
    # имя_dc так же будет и в словаре_с_документацией_dc, но такое дублирование
    # применено сознательно - для недопущения дублей (это упростит дальнейшую
    # обработку при формировании документации).
    all_dc = {}

    # 1. Данные для страницы со списком компонентов
    data_list_rpc_components = []

    for rpc_component in rpc_components:

        data_list_rpc_components.append(
            {
                'name': rpc_component.name,
                'url': make_rpc_component_url(rpc_component.name)
            }
        )

        # 2. Данные для страницы компонента
        data_rpc_component = {}
        data_rpc_component['name'] = rpc_component.name
        data_rpc_component['doc_str'] = make_str_p_html(rpc_component.__doc__)
        list_metgods = []
        rpc_component_api_doc = rpc_component.get_api_doc()
        for method in rpc_component_api_doc:
            list_metgods.append(
                {
                    'name': method['method_name'],
                    'url': make_method_url(method['method_name'])
                }
            )

            # 3. Данные для страницы метода
            data_method = {}
            data_method['name'] = method['method_name']
            data_method['doc_str'] = make_str_p_html(method['method_doc_str'])

            for io in ('input', 'output'):
                data_class_name = method[io + '_dc']['class_name']
                data_method[io] = data_class_name
                data_method[io + '_url'] = make_data_class_url(data_class_name)
                # так же, соберем все датаклассы
                all_dc[data_class_name] = method[io + '_dc']

            # 3. Запись страницы метода
            data = {'method': data_method}
            template = 'method.html'
            path = DOCS + DOCS_RPC_COMPONENTS + rpc_component.name
            path += '/' + data_method['name'].split('.')[1] + '.html'
            save_doc(data, template, path)

        data_rpc_component['methods'] = list_metgods

        # 2. Запись страницы компонента
        data = {'rpc_component': data_rpc_component}
        template = 'rpc_component.html'
        path = DOCS + DOCS_RPC_COMPONENTS + rpc_component.name
        path += '/index.html'
        save_doc(data, template, path)

    #  1. Запись страницы со списком компонентов
    data = {'rpc_components': data_list_rpc_components}
    template = 'list_rpc_components.html'
    path = DOCS + 'list_rpc_components.html'
    save_doc(data, template, path)

    # Далее происходит запись датаклассов.
    # Использовать надо словарь all_dc плюс датаклассы из DC_EXTRA

    # Добавим в словарь all_dc документацию из датаклассов из DC_EXTRA
    for dc_extra in DC_EXTRA:
        doc_dc_extra = dc_extra.get_doc()
        all_dc[doc_dc_extra['class_name']] = doc_dc_extra

    # Сформируем список из ключей
    dc_names = list(all_dc.keys())
    dc_names.sort(key=len)
    # Сначала будут самые длинные имена (это необходимо для исключения
    # ошибочной замены имени, например, когда есть FooDC и FooFooDC)
    dc_names.reverse()

    def type_final(type_: type) -> str:
        """
            Функция будет вызываться из шаблона.
            Заменяет в типе имя класса на html строку со ссылкой на страницу
            документации класса.
        """
        str_type = type_readable_form(type_)
        index_unique_mark = 0

        finded_dc = []
        # Поищем датаклассы в str_type (в строковом представлении типа поля)
        for dc_name in dc_names:
            i = str_type.find(dc_name)
            if i > -1:
                # Имя датакласса найдено в строке типа поля
                # 1. Получим для датакласса уникальную метку
                index_unique_mark += 1
                unique_mark = '#%s#' % index_unique_mark
                # 2. Добавим в список найденных датаклассов кортеж
                # вида ('имя_датакласса', 'уникальная_метка')
                finded_dc.append((dc_name, unique_mark))
                # 3. Заменим в исходной строке типа поля имя датакласса на
                # его уникальную метку (для возможности обратной замены после
                # полного сканирования строки типа поля)
                str_type = str_type.replace(dc_name, unique_mark)

        if finded_dc:
            # Заменим в исходной строке все метки датаклассов на html строки
            # содержащие и ссылки на эти датаклассы
            for dc_info in finded_dc:
                dc_name, dc_unique_mark = dc_info
                dc_url = dc_name + '.html'
                dc_html = '<a href="%s">%s</a>' % (dc_url, dc_name)
                str_type = str_type.replace(dc_unique_mark, dc_html)

        return str_type

    for dc in all_dc.values():
        # Данные для страницы датаклассов
        data_dc = {}
        data_dc['name'] = dc['class_name']
        data_dc['doc_str'] = make_str_p_html(dc['class_doc_str'])
        data_dc['class_fields'] = dc['class_fields']

        # Запись страницы датаклассов
        data = {
            'dc': data_dc,
            'value_readable_form': value_readable_form,
            'type_final': type_final
        }
        template = 'data_class.html'
        path = DOCS + DOCS_DATA_CLASSES + data_dc['name'] + '.html'

        save_doc(data, template, path)


if __name__ == "__main__":
    create_doc()
